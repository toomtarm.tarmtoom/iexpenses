var database = require('./database')

const express = require('express')
const { get } = require('js-cookie')
const app = express()
app.get('/hello', (req, res) => {
  res.json(database.findAll())
})
app.get('/hello/:id', (req, res, next) => {
  res.json(
    expenseslist.find((expenseslist) => expenseslist.id === req.params.id)
  )
})
app.get('/test/:uid1/between/:uid2', (req, res) => {
  var val = req.params.uid2
  console.log(req.route.uid1)
  // res.json(database.findById(val))
  res.json(val)
})

//allData
app.get('/expenses', (req, res) => {
  res.json(database.findAll())
})

//Today ผลรวมทั้งหมดของ Today แยกตามประเภท
app.get('/expenses/typeOfToday/:val', (req, res) => {
  var val = req.params.val
  res.json(database.typeOfToday(val))
})

app.get('/expenses/totalAllDay', (req, res) => {
  res.json(database.totalAllDay())
})

app.get('/expenses/todaylist/', (req, res) => {
  var val = req.params.id
  // res.json(database.findById(val))
  res.json(database.todayType())
})

// รวมค่าใช่ค่าย ต่อวัน ต่อสัปดาห์ โดยค้นหาวันที่
app.get('/expenses/totaldailyofweek/:val', (req, res) => {
  var val = req.params.val
  // res.json(database.findById(val))
  res.json(database.totalDailyofWeek(val))
})

//แสดง List ทั้งหมด โดยค้นหาวันที่
app.get('/expenses/todaylist/:val', (req, res) => {
  var val = req.params.val
  // res.json(database.findById(val))
  res.json(database.listToday(val))
})

app.get('/expenses/totalallmulti/:val1&:val2', (req,res) =>{
  var valStart = req.params.val1
  var valLast = req.params.val2
  res.json(database.totalAllMulti(valStart,valLast))
})

app.get('/expenses/totaltypemulti/:val1&:val2', (req,res) =>{
  var valStart = req.params.val1
  var valLast = req.params.val2
  res.json(database.totalTypeMulti(valStart,valLast))
})

app.get('/expenses/typeexpensesname/', (req,res) => {
  res.json(database.typeExpensesName())
})


app.get('/expenses/weekly/', (req, res) => {
  res.json(database.weekly())
})
module.exports = app
