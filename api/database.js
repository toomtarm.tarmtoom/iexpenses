import { object } from 'firebase-functions/lib/providers/storage'


var dummy = require('./dummyData')
var typeName = require('./typeName')
var dataExpensestype = require('./darftExpensestype')
var myFunction = require('./function')

// const config = {
//   apiKey: 'AIzaSyBiXPxfUOFD_avpAD8T7UcdMaEaKuMBeJQ',
//   authDomain: 'iexpenses-project.firebaseapp.com',
//   databaseURL: 'https://iexpenses-project.firebaseio.com',
//   projectId: 'iexpenses-project',
//   storageBucket: 'iexpenses-project.appspot.com',
//   messagingSenderId: '421169111251',
//   appId: '1:421169111251:web:69118aed33fb7add07f426',
//   measurementId: 'G-NBGT3TB07F',
// }

// !firebase.apps.length ? firebase.initializeApp(config) : ''
// export const auth = firebase.auth()
// export const db = firebase.firestore()
// var expenseslist = []

// db.collection('expensesTable')
//   .get()
//   .then((querySnapshot) => {
//     querySnapshot.forEach((doc) => {
//       var whereID = doc.data().exlistID
//       var whereUnitID = doc.data().exunitID
//       var whereStoresID = doc.data().exstoresID

//       db.collection('expensesList')
//         .doc(`${whereID}`)
//         .get()
//         .then((doc1) => {
//           var whereTypeID = doc1.data().extypeID
//           db.collection('expensesStores')
//             .doc(`${whereStoresID}`)
//             .get()
//             .then((doc2) => {
//               db.collection('expensesUnit')
//                 .doc(`${whereUnitID}`)
//                 .get()
//                 .then((doc3) => {
//                   db.collection('expensesType')
//                     .doc(`${whereTypeID}`)
//                     .get()
//                     .then((doc4) => {
//                       const currentData = {
//                         id: doc.id,
//                         no: doc.data().extableNo,
//                         listExpenses: {
//                           id: doc1.id,
//                           name: doc1.data().exlistName,
//                           detail: doc1.data().exlistDetail,
//                         },
//                         typeExpenses: {
//                           id: doc4.id,
//                           name: doc4.data().extypeName,
//                           detail: doc4.data().extypeDetail,
//                         },
//                         storesExpenses: {
//                           id: doc2.id,
//                           name: doc2.data().exstoresName,
//                           detail: doc2.data().exstoresDetail,
//                           location: doc2.data().exstoresLocation,
//                         },
//                         amount: doc.data().extableAmount,
//                         unitExpenses: {
//                           id: doc3.id,
//                           name: doc3.data().name,
//                         },
//                         price: doc.data().extablePrice,
//                         date: doc.data().extableDate,
//                         detail: doc.data().extableDetail,
//                         auth: doc.data().auth,
//                       }
//                       expenseslist = [...expenseslist, currentData]
//                     })
//                 })
//             })
//         })
//     })
//   })

exports.findAll = function () {
  return dummy.dataExpenses
}

// exports findID(){
// }
exports.findById = function (id) {
  //   for (var i = 0; i < expenseslist.length; i++) {
  //     if (expenseslist[i].id == id) return expenseslist[i]
  //   }
  return id
}

exports.dateToday = function (val) {
  return dummy.dataExpenses.filter((res) => res.date.includes(val))
}

// สำหรับกราฟ Stack 100
exports.todayType = function () {
  //เลือกชื่อประเภท
  let typeName = []
  dummy.dataExpenses.forEach(
    (data) => (typeName = [...typeName, data.typeExpenses.name])
  )
  //คัดชื่อซ้ำออก

  typeName = [...new Set(typeName)]
  let test = []
  let endData = []
  typeName.forEach((resdata) => {
    test = dummy.dataExpenses.filter((res) =>
      res.typeExpenses.name.includes(resdata)
    )
    let sumTotol = test.reduce((prev, { price }) => prev + price, 0)
    const currentendData = {
      totol: sumTotol,
      typename: resdata,
    }
    endData = [...endData, currentendData]
  })
  // return typeName

  return endData.sort((a, b) =>
    a.totol > b.totol ? -1 : a.totol < b.totol ? 1 : 0
  )
}

// dateVal = () => {
//   const today1 = new Date().toISOString().slice(0, 10)
//   const today2 = new Date(today1).getTime()
//   return today2
// }

const typeOfToday = function (valTime) {
  const heckAdult = (val) => {
    return val.date == valTime
  }
  const dateFilter = dummy.dataExpenses.filter(heckAdult)
  let typeName = []
  dateFilter.forEach((data) => {
    const newObj = {
      id: parseInt(data.typeExpenses.id),
      name: data.typeExpenses.name,
      price: data.price,
    }
    typeName = [...typeName, newObj]
  })
  typeName = [...new Set(typeName)]

  var arr = typeName.reduce((acc, item) => {
    let existItem = acc.find(({ id }) => item.id === id)
    if (existItem) {
      existItem.price += item.price
    } else {
      acc.push(item)
    }
    return acc
  }, [])

  const actual = myFunction.mergeArrays(dataExpensestype.draftObj, arr)
  return actual
}

const totalAllMulti = function (valStart, valLast) {
  // let date1 = new Date(valStart)
  // let date2 = new Date(valLast)
  // return valStart + " To " + valLast

  const heckAdult = (val) => {
    return val.date >= valStart && val.date <= valLast
  }
  const dateFilter = dummy.dataExpenses.filter(heckAdult)
  let typeName = []
  dateFilter.forEach((data) => {
    const newObj = {
      id: parseInt(data.typeExpenses.id),
      name: data.typeExpenses.name,
      date: data.date,
      price: data.price,
    }
    typeName = [...typeName, newObj]
  })
  typeName = [...new Set(typeName)]

  var arr = typeName.reduce((acc, item) => {
    let existItem = acc.find(({ id }) => item.id === id)
    if (existItem) {
      existItem.price += item.price
    } else {
      acc.push(item)
    }
    return acc
  }, [])

  const actual = myFunction.mergeArrays(dataExpensestype.draftObj, arr)
  return actual
}

// สำหรับกราฟ แท่ง แบบ List
exports.todayList = function () {
  //เลือกชื่อประเภท
  let typeName = []
  dummy.dataExpenses.forEach(
    (data) => (typeName = [...typeName, data.typeExpenses.name])
  )

  //คัดชื่อซ้ำออก
  typeName = [...new Set(typeName)]
  let test = []
  let endData = []
  typeName.forEach((resdata) => {
    test = dummy.dataExpenses.filter((res) =>
      res.typeExpenses.name.includes(resdata)
    )
    let sumTotol = test.reduce((prev, { price }) => prev + price, 0)
    const currentendData = {
      totol: sumTotol,
      typename: resdata,
    }
    endData = [...endData, currentendData]
  })

  return endData.sort((a, b) =>
    a.totol > b.totol ? -1 : a.totol < b.totol ? 1 : 0
  )
}

exports.weekly = function () {
  let date1 = 1606003200000 // อดีต
  let date2 = 1606089600000 // ปัจจุบัน
  const dateFunction = myFunction.filterdate(dummy.dataExpenses, date1, date2)
  return dateFunction
}
// 1606089600000
// 1606978795660

const totalAllDay = function () {
  let typeName = []
  dummy.dataExpenses.forEach(
    (data) =>
      (typeName = [
        ...typeName,
        {
          id: data.typeExpenses.id,
          name: data.typeExpenses.name,
          date: data.date,
          price: data.price,
        },
      ])
  )

  // var result = typeName.reduce(function (r, a) {
  //   r[a.date] = r[a.date] || []
  //   r[a.date].push(a)
  //   return r
  // }, Object.create(null))

  var helper = {}
  var resultVal = typeName.reduce(function (prev, cur) {
    var key = cur.date + '-' + cur.id

    if (!helper[key]) {
      helper[key] = Object.assign({}, cur) // create a copy of o
      prev.push(helper[key])
    } else {
      helper[key].price += cur.price
    }

    return prev
  }, [])
  // let result = Object.keys(resultVal).map((date) => resultVal[date])
  return resultVal.sort((a, b) =>
    a.date > b.date ? -1 : a.date < b.date ? 1 : 0
  )
}

// รวมค่าใช่ค่าย ต่อวัน ต่อสัปดาห์ โดยค้นหาวันที่
const totalDailyofWeek = function (val) {
  const data = dummy.dataExpenses
  const dateMain = val
  const dateSec = val - 86400000 * 6
  //86400000 = 1 วัน

  const dateFunction = myFunction.filterdate(
    dummy.dataExpenses,
    dateSec,
    dateMain
  )

  // จัดกรุ๊ปใหม่
  let newObj = []
  dateFunction.forEach((data) => {
    newObj = [
      ...newObj,
      {
        id: data.typeExpenses.id,
        name: data.typeExpenses.name,
        date: data.date,
        price: data.price,
      },
    ]
  })

  // คำนวนผลรวมแยกตาม วันที่ และ ไอดี ประเภทค่าใช้จ่าย
  var helper = {}
  var resultVal = newObj.reduce(function (prev, cur) {
    var key = cur.date + '-' + cur.id

    if (!helper[key]) {
      helper[key] = Object.assign({}, cur) // create a copy of o
      prev.push(helper[key])
    } else {
      helper[key].price += cur.price
    }

    return prev
  }, [])

  let result = resultVal.reduce(
    (prev, cur) => ({
      ...prev,
      [cur.date]: (prev[cur.date] || []).concat(cur),
    }),
    {}
  )

  // const actual = myFunction.mergeArrays(dataExpensestype.draftObj, arr)

  return resultVal
}

const totalTypeMulti = (val1, val2) => {
  

  const dateFunction = myFunction.filterdate(dummy.dataExpenses, val1, val2)

  // จัดกรุ๊ปใหม่
  let newObj = []
  dateFunction.forEach((data) => {
    newObj = [
      ...newObj,
      {
        id: data.typeExpenses.id,
        name: data.typeExpenses.name,
        date: data.date,
        price: data.price,
      },
    ]
  })

  // คำนวนผลรวมแยกตาม วันที่ และ ไอดี ประเภทค่าใช้จ่าย
  var helper = {}
  var resultVal = newObj.reduce(function (prev, cur) {
    var key = cur.date + '-' + cur.id

    if (!helper[key]) {
      helper[key] = Object.assign({}, cur) // create a copy of o
      prev.push(helper[key])
    } else {
      helper[key].price += cur.price
    }
    return prev
  }, [])
  return resultVal
}

const typeExpensesName = function () {
  return typeName.typeName
}

//แสดง List ทั้งหมด โดยค้นหาวันที่
const listToday = function (val) {
  const today3 = val
  const heckAdult = (val) => {
    return val.date == today3
  }
  const dateFilter = dummy.dataExpenses.filter(heckAdult)
  return dateFilter
}
module.exports = {
  listToday, //แสดง List ทั้งหมด โดยค้นหาวันที่
  totalDailyofWeek, // รวมค่าใช่จ่ายทั้งหมด ต่อวัน ต่อสัปดาห์ โดยค้นหาวันที่
  totalTypeMulti, // รวมค่าใช่จ่ายแบ่งแยกตามประเภท  ต่อวัน ต่อสัปดาห์ โดยค้นหาวันที่ (แยกตามวัน)
  typeOfToday, // ผลรวมทั้งหมดของ Today แยกตามประเภท
  totalAllMulti, // ผลรวมทั้งหมด สามารถแยกได้ Weekly Monthly Yearly (ไม่แยกตามวัน) ุถ้าคนหาทั้งเดือน ก็รวมแยกประเภทมาก้อนเดียว
  totalAllDay, // ผลรวมทั้งหมด โดยแยกตามวันที่ และประเภท และไม่มีเงื่อนไขในการค้นหา
  typeExpensesName, // แสดงเฉพาะ Type name
}
