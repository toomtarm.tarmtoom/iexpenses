exports.draftObj = [
  {
    id: 1,
    name: 'น้ำหน้า (เฉาก๊วย)',
    price: 0,
  },
  {
    id: 2,
    name: 'เฉาก๊วย',
    price: 0,
  },
  {
    id: 3,
    name: 'ขนม',
    price: 0,
  },
  {
    id: 4,
    name: 'เฉาก๊วย + น้ำหน้า (เฉาก๊วย)',
    price: 0,
  },
  {
    id: 5,
    name: 'อื่นๆ',
    price: 0,
  },
  {
    id: 6,
    name: 'เฉาก๊วย (รวม) + ขนม',
    price: 0,
  },
]
