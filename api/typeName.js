exports.typeName = [
  {
    id: '1',
    name: 'น้ำหน้า (เฉาก๊วย)',
    detail: 'น้ำหน้า (เฉาก๊วย) หรือ น้ำนมสดสำหรับราดเฉาก๊วย',
  },
  { id: '2', name: 'เฉาก๊วย', detail: 'เฉาก๊วย หรือตัวเนื้อเฉาก๊วย' },
  {
    id: '3',
    name: 'ขนม',
    detail: 'ขนมทั้งหมด เช่น ปังกรอบ คุกกี๊ หรือ อื่นๆ',
  },
  {
    id: '4',
    name: 'เฉาก๊วย + น้ำหน้า (เฉาก๊วย)',
    detail:
      'เฉาก๊วย + น้ำหน้า (เฉาก๊วย) รวมทั้ง 2 ที่ไม่สามารถแยกค่าใช้จ่ายได้ได้',
  },
  {
    id: '6',
    name: 'เฉาก๊วย (รวม) + ขนม',
    detail: 'เฉาก๊วย (รวม) + ขนม รวมกันทั้งหมดที่ไม่สามารถแยกได้',
  },
  { id: '5', name: 'อื่นๆ', detail: 'ค่าน้ำ ค่าไฟ ค่าน้ำมัน หรือ ค่าอื่นๆ ' },
]
