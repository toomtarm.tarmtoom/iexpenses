exports.mergeArrays = (arr1 = [], arr2 = []) => {
  //Creates an object map of id to object in arr1
  const arr1Map = arr1.reduce((acc, o) => {
    acc[o.id] = o
    return acc
  }, {})
  //Updates the object with corresponding id in arr1Map from arr2,
  //creates a new object if none exists (upsert)
  arr2.forEach((o) => {
    arr1Map[o.id] = o
  })

  //Return the merged values in arr1Map as an array

  return Object.values(arr1Map).sort((a, b) =>
    a.id < b.id ? -1 : a.id > b.id ? 1 : 0
  )
}

exports.filterdate = (val, date1, date2) => {

  let firstDate = parseInt(date1)
  let lastDate = parseInt(date2) + 86390000
  
  const dateFind = val.filter((dataval) => {
    return (dataval.date >= firstDate && dataval.date <= lastDate)
  })

  return dateFind
}
