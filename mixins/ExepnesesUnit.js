import { mapGetters, mapActions } from 'vuex'
export default {
  data() {
    return {
      dataExpensesUnit: [],
    }
  },
  watch: {
    getExpensesUnit() {
      this.dataExpensesUnit = this.getExpensesUnit
    },
  },
  computed: {
    ...mapGetters({
      getExpensesUnit: 'getExpensesUnit',
    }),
  },
  mounted() {
    this.callUnitAll()
  },
  methods: {
    ...mapActions({
      callUnitAll: 'callUnitAll',
    }),
  },
}
