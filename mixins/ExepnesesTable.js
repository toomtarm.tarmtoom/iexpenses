import { mapGetters, mapActions, mapState } from 'vuex'
export default {
  data() {
    return {
      dataExpensesTable: [],
    }
  },
  watch: {
    getExpensesTable() {
      this.dataExpensesTable = this.getExpensesTable
    },
  },
  computed: {
    // ...mapGetters({
    //   getExpensesTable: 'getExpensesTable',
    // }),
  },
  mounted() {
    this.callTableAll()
  },
  methods: {
    ...mapActions({
      callTableAll: 'callTableAll',
    }),
  },
}
