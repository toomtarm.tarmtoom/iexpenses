import { mapGetters, mapActions } from 'vuex'

export default {
  components: {},
  data() {
    return {}
  },
  computed: {
    ...mapGetters({
      imageData: 'getImageList',
    }),
    itemUnit() {
      return this.imageData
    },
  },
  mounted() {
    this.callImageAll()
    this.callTableAll()
  },
  methods: {
    ...mapActions({
      callImageAll: 'callImageAll',
      callTableAll: 'callTableAll'
    }),
  },
  
}
