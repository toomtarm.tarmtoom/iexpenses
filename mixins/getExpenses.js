import { mapGetters, mapActions, mapState } from 'vuex'
export default {
  data() {
    return {
      dataExpensesList: [],
      dataExpensesUnit: [],
      dataExpensesTable: [],
      dataExpensesStores: [],
      datadashboardTime: [],
    }
  },
  watch: {
    getExpensesList() {
      this.dataExpensesList = this.getExpensesList
    },
    getExpensesUnit() {
      this.dataExpensesUnit = this.$nuxt.$store.state.expensesunit.ExpensesUnit
    },
    getExpensesTable() {
      this.dataExpensesTable = this.$nuxt.$store.state.expensestable.ExpensesTable
    },
    getExpensesStores() {
      this.dataExpensesStores = this.$nuxt.$store.state.expensesstores.ExpensesStores
    },
    getdashboardTime() {
      this.getDataAxios(this.$nuxt.$store.state.template.dashboardTime)
    },
  },
  computed: {
    ...mapGetters({
      getExpensesList: 'getExpensesList',
      // getExpensesUnit: 'getExpensesUnit',
      // getExpensesTable1: 'getExpensesTable',
    }),
    ...mapState({
      getExpensesTable: (state) => state.expensestable.ExpensesTable,
      getExpensesStores: (state) => state.expensesstores.ExpensesStores,
      getdashboardTime: (state) => state.template.dashboardTime,
      getExpensesUnit: (state) => state.expensesunit.ExpensesUnit
    }),
  },
  mounted() {
    this.callListAll(),
      // this.callTableAll(),
      this.callUnitAll()
    this.callStoreAll()
  },
  methods: {
    ...mapActions({
      callListAll: 'callListAll',
      // callTableAll: 'callTableAll',
      callUnitAll: 'callUnitAll',
      callStoreAll: 'callStores',
    }),
  },
}
