import { mapGetters, mapActions, mapState } from 'vuex'
export default {
  data() {
    return {
      datadashboardTime: [],
      dataExpenses: [],
    }
  },
  watch: {
    getdashboardTime() {
      // this.getDataAxios(
      //   this.$nuxt.$store.state.template.dashboardTime,
      //   this.$nuxt.$store.state.expensestable.ExpensesTable
      // )
    },
  },
  computed: {
    ...mapGetters({}),
    ...mapState({
      getdashboardTime: (state) => state.template.dashboardTime,
      getExpensesTable: (state) => state.expensestable.ExpensesTable,
      getExpensesType: (state) => state.expensestype.ExpensesType,
    }),
  },
  mounted() {},
  methods: {

    formateDate(val, date1, date2) {
      let dateArr = []
      for (date2; date2 <= date1; date2 += 86400000) {
        dateArr = [
          ...dateArr,
          new Date(date2).toLocaleDateString('th-TH', {
            year: 'numeric',
            month: 'long',
            day: '2-digit',
          }),
        ]
      }
      return dateArr
    },
    formateWeek(val, date1, date2) {
      let dateArr = []
      for (date2; date2 <= date1; date2 += 86400000 * 7) {
        var getWeek = this.getWeekNumberNonISO(new Date(date2))
        dateArr = [...dateArr, getWeek]
      }
      return dateArr
    },
    getWeekNumberNonISO(d) {
      var sat = new Date(
        Date.UTC(d.getFullYear(), d.getMonth(), d.getDate(), 23, 59, 59, 999)
      )

      sat.setUTCDate(sat.getUTCDate() + 6 - sat.getUTCDay())

      var firstDay = new Date(Date.UTC(sat.getUTCFullYear(), 0, 1))

      firstDay = firstDay.setUTCDate(
        firstDay.getUTCDate() - firstDay.getUTCDay()
      )

      return Math.round((sat - firstDay) / 6.048e8)
    },
    moneyFormatter(moneyVal) {
      var formatter = new Intl.NumberFormat('th-TH', {
        style: 'currency',
        currency: 'THB',
      })
      return formatter.format(moneyVal)
    },
    onefunction(val, date1, date2) {
      let dateArr = []
      for (date2; date2 <= date1; date2 += 86400000 * val) {
        dateArr = [...dateArr, date2]
      }
      return dateArr
    },
    setObjectBG(countName) {
      let setArrayBGColor = []
      for (var i = 0; i < countName; i++) {
        const dataSet = this.bgColor(i)
        setArrayBGColor = [...setArrayBGColor, dataSet]
      }
      return setArrayBGColor
    },
    bgColor(val) {
      var c = [
        '#3CD4A0',
        'rgb(255, 194, 96)',
        'rgb(255, 92, 147)',
        '#8a18e7',
        'rgb(83, 109, 254)',
        '#78eb6a',
        '#f1c732',
        '#f73b45',
        '#b430c5',
        '#214e64',
      ]
      for (var i = 0; i <= 5; i++) {
        if (val == i) {
          return c[i]
        }
      }
    },
    totalTypeMulti(dataRes, dataStart, dateEnd) {
      const dateFunction = this.filterdate(dataRes, dataStart, dateEnd)

      // จัดกรุ๊ปใหม่
      let newObj = []
      dateFunction.forEach((data) => {
        newObj = [
          ...newObj,
          {
            id: data.typeExpenses.id,
            name: data.typeExpenses.name,
            date: new Date(data.date).getTime(),
            price: data.price,
          },
        ]
      })

      // คำนวนผลรวมแยกตาม วันที่ และ ไอดี ประเภทค่าใช้จ่าย
      var helper = {}
      var resultVal = newObj.reduce(function (prev, cur) {
        var key = cur.date + '-' + cur.id

        if (!helper[key]) {
          helper[key] = Object.assign({}, cur) // create a copy of o
          prev.push(helper[key])
        } else {
          helper[key].price += cur.price
        }
        return prev
      }, [])
      return resultVal
    },
    filterdate(dataRes, dataStart, dateEnd) {
      let firstDate = parseInt(dataStart)
      let lastDate = parseInt(dateEnd) + 86390000
      const dateFind = dataRes.filter((dataval) => {
        return (
          new Date(dataval.date).getTime() >= firstDate &&
          new Date(dataval.date).getTime() <= lastDate
        )
      })

      return dateFind
    },
  },
}
