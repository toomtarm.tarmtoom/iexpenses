export default function ({ store, route, redirect }) {
  const user = store.state.users.user
  const profile = store.state.profile
  const blockedindex = /\/\/*/g
  const blockedprofile = /\/profile\/*/g
  const blockedRoute = /\/iexpenses\/*/g
  const blockedDashboard = /\/dashboard\/*/g
  const formLogin = /\/formLogin\/*/g
  // const blockedRoute2 = /\/test\/*/g;
  const homeRoute = '/'

  if (!user && route.path.match(blockedindex)) {
    redirect('/formLogin')
  }
  if (!user && route.path.match(blockedprofile)) {
    redirect('/formLogin')
  }
  if (!user && route.path.match(blockedRoute)) {
    redirect('/formLogin')
  }
  if (!user && route.path.match(blockedDashboard)) {
    redirect('/formLogin')
  }

  // if (!user && route.path.match(blockedRoute2)) {
  //   redirect("/");
  // }

  if (user && route.path === homeRoute) {
    redirect('/profile')
  }
  if (user && route.path.match(formLogin)) {
    redirect('/profile')
  }
}
