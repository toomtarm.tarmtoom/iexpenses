import Vuex from 'vuex'

import templateModule from './modules/template'
import cookieModule from './modules/users/cookie'
import userModule from './modules/users/users'
import expensestype from './modules/expenses/type'
import expenseslist from './modules/expenses/list'
import expensestable from './modules/expenses/table'
import expensesunit from './modules/expenses/unit'
import expensesstores from './modules/expenses/stores'
import imageStorage from './modules/imageFile/image'
import profile from './modules/users/profile'

const store = () => {
  return new Vuex.Store({
    namespaced: true,
    modules: {
      template: templateModule,
      cookie: cookieModule,
      users: userModule,
      expensestype,
      expenseslist,
      expensestable,
      expensesunit,
      expensesstores,
      imageStorage,
      profile:profile
    },
  })
}

export default store
