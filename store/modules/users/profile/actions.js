import { db, auth } from '@/services/firebase'

export default {
  callProfile: ({ commit }) => {
    auth.onAuthStateChanged((user) => {
      db.collection('profile')
        .where('email', '==', `${user.email}`)
        .onSnapshot((querySnapshot) => {
          querySnapshot.forEach((doc) => {
            commit('SET_PROFILE', {
              name: doc.data().name,
              email: doc.data().email,
              birthday: doc.data().birthday,
              imageUrl: doc.data().imageUrl,
            })
          })
        })
    })
  },
  addProfile({ commit }, val) {
    try {
      const objForsave = {
        name: val.formVal.name,
        email: val.formVal.email,
        birthday: val.formVal.birthday,
        imageUrl: val.urlVal,
      }

      auth.onAuthStateChanged((user) => {
        db.collection('profile')
          .doc(`${user.uid}`)
          .set(objForsave)
          .then(() => {
            console.log('Serve ExpensesUnit Sussess')
            commit('SET_PROFILE', objForsave)
          })
          .catch((err) => console.log('Serve ADD ExpensesType Error :', err))
      })
    } catch (error) {
      throw error
    }
  },
}
