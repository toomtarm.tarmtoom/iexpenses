export default {
  SET_DRAWER: (state, newDrawer) => {
    state.drawer = newDrawer
  },
  SET_DASHBOARDTIME: (state, newVal) =>{
    state.dashboardTime = newVal
  }
}