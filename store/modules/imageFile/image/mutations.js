export default {
  SET_IMAGELIST: (state, newVal) => {
    state.imageList = newVal
  },

  SET_IMAGELIST_ADD: (state, newVal) => {
    state.imageList.push(newVal)
  },
  SET_IMAGELIST_DEL: (state, newObj) => {
    state.imageList = state.imageList.filter((x) => x.name != newObj)
  },
}
