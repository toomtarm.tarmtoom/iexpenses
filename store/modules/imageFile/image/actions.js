import { storage, auth } from '@/services/firebase'

export default {
  callImageAll: ({ commit }) => {
    auth.onAuthStateChanged((user) => {
      const userID = user.uid
      const listRef = storage.child(`mainBucket/bill/${userID}`)
      let dataAr = []
      listRef
        .listAll()
        .then((res) => {
          res.items.forEach((itemRef) => {
            const pathVal = itemRef.fullPath
            const starsRef = storage.child(`${pathVal}`)
            starsRef
              .getDownloadURL()
              .then((url) => {
                const currentData = {
                  name: itemRef.name,
                  link: url,
                }
                dataAr = [...dataAr, currentData]
                commit('SET_IMAGELIST', dataAr)
              })
              .catch((error) => {
                console.log('SERVER ERROR', error)
              })
          })
        })
        .catch((error) => {
          // Uh-oh, an error occurred!
        })
    })
  },
  addImage({ commit }, val) {
    try {
      auth.onAuthStateChanged((user) => {
        const userID = user.uid
        const listRef = storage.child(
          `mainBucket/bill/${userID}/${val.imageObj.imageName}`
        )
        listRef
          .put(val.imageObj.imageFile)
          .then((response) => {
            response.ref.getDownloadURL().then((url) => {
              const dataObj = {
                name: response.ref.name,
                link: url,
              }
              commit('SET_IMAGELIST_ADD', dataObj)
            })
          })
          .catch((error) => {
            // Uh-oh, an error occurred!
          })
      })
    } catch (error) {
      throw error
    }
  },
  delImage({ commit }, val) {
    try {
      auth.onAuthStateChanged((user) => {
        const userID = user.uid
        storage
          .child(`mainBucket/bill/${userID}/${val}`)
          .delete()
          .then((res) => {
            commit('SET_IMAGELIST_DEL', val)
          })
          .catch((error) => {
            // Uh-oh, an error occurred!
          })
      })
    } catch (error) {
      throw error
    }
  },
}
