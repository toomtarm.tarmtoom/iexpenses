export default {
  SET_Expenses_Unit: (state, newTypeAll) => {
    state.ExpensesUnit = newTypeAll
  },
  SET_Expenses_Unit_LastID: (state, newID) => {
    //ค้นหา ID ล่าสุด
    state.ListID = newID
  },

    // เพิ่ม 4.2 (store->Expensese->List-Mutation) เพิ่มข้อมูลเข้า State
  SET_Expenses_Unit_Add: (state, newVal) => {
     // เพิ่มข้อมูล state.ExpensesType
    state.ExpensesUnit.push(newVal)
  },
    //เพิ่ม 4.3 (store->Expensese->List-Mutation) เพิ่ม ID + 1 ล่าสุด  เข้า State สำหรับ ID 
  SET_Expenses_Unit_LastID1: (state, newVal) => {
    //ค้นหา ID ล่าสุด +1
    state.ListID = newVal + 1
  },

    SET_Expenses_Unit_EditItem: (state, newItem) =>{
    //จัดการระหว่างเพิ่ม/แก้ไขข้อมูล  เพิ่ม -1 / แก้ไข 1
    state.EditItem = newItem
  },
  SET_Expenses_Unit_EditData: (state, newVal) =>{
    state.EditData = newVal // สำหรับส่งข้อมูลข้า Component
  },
  SET_Expenses_Unit_Delete: (state, newObj) => {
    const indexNumber = state.ExpensesUnit.findIndex((stateIndex) => stateIndex.id === newObj.id) // หา Index แล้ว -1
    state.ExpensesUnit.splice(indexNumber, 1)
  },
  
  SET_Expenses_Unit_Update: (state, newObj) => { // แก้ไข อัพเดท state.ExpensesList
    const indexNumber = state.ExpensesUnit.findIndex((stateIndex) => stateIndex.id === newObj.id) // หา Index แล้ว -1
    Object.assign(state.ExpensesUnit[indexNumber], newObj)
  },
    /*


     Template
   */

  //เพิ่ม 4.1 (store->Expensese->List-Mutation) ปิดหน้า Form

  SET_FORM_EXPENSES_UNIT: (state, newExpand) => {
    // Toggle Component
    state.formExpensesUnit = newExpand
  },
}
