import { db, auth } from '@/services/firebase'

export default {
  callUnitAll: ({ commit }, id) => {
    auth.onAuthStateChanged((user) => {
      db.collection('expensesUnit')
        .where('auth', 'array-contains', `${user.email}`)
        // .where('auth', '==', `${user.email}`)
        .onSnapshot((querySnapshot) => {
          let dataAr = []
          querySnapshot.forEach((doc) => {
            const currentData = {
              id: doc.id,
              name: doc.data().name,
            }
            dataAr = [...dataAr, currentData]
            commit('SET_Expenses_Unit', dataAr)
          })
        })
    })
  },
  findID: ({ commit }) => {
    db.collection('expensesUnit')
      .get()
      .then((querySnapshot) => {
        if (!querySnapshot.empty) {
          let last = []
          querySnapshot.forEach((doc) => {
            last = [...last, parseInt(doc.id)]
          })
          commit(
            'SET_Expenses_Unit_LastID',
            last.reduce((prev, cur) => (cur > prev ? cur : prev)) + 1
          )
        } else {
          var valID = 1
          console.log('No Data', valID)
          commit('SET_Expenses_Unit_LastID', valID)
        }
      })
  },
  async addDataExpensesUnit(
    {
      //เพิ่มข้อมูล
      commit,
    },
    val
  ) {
    try {
      //เพิ่ม 3.1 (store->Expensese->List-Action) เซ็ทค่าเข้าไปใน Object สำหรับ บันทึกลง Database
      const objForsave = {
        name: val.formVal.name,
        auth: [val.formVal.auth],
      }

      /*เพิ่ม 3.2 (store->Expensese->List-Action) เซ็ทค่าเข้าไปใน Object สำหรับ Commit เข้า State
        หมายเหตุ ไม่สามารถ Commit val ตรงๆได้เนื่องจากมี Data val.typeExpenses.auth อยู่ 
      */
      const objForcommit = {
        id: val.formVal.id,
        name: val.formVal.name,
        auth: [val.formVal.auth],
      }

      //เพิ่ม 3.3 (store->Expensese->List-Action) คำสั่งบันทึกลงใน Database
      await db
        .collection('expensesUnit')
        .doc(`${val.formVal.id}`)
        .set(objForsave)
        .then(() => {
          console.log('Serve ExpensesUnit Sussess')

          //เพิ่ม 3.3.1 (store->Expensese->List-Action) Set Commit สำหรับเพิ่มข้อมูล ExpensesList
          commit('SET_Expenses_Unit_Add', val.formVal)

          //เพิ่ม 3.3.2 (store->Expensese->List-Action) Set Commit จัดการ ID ล่าสุด  เข้า State สำหรับ ID
          commit('SET_Expenses_Unit_LastID1', objForcommit)
        })
        .catch((err) => console.log('Serve ADD ExpensesType Error :', err))
    } catch (error) {
      throw error
    }
  },
  async editDataExpensesUnit({ commit }, val) {
    try {
      //เพิ่ม 3.1 (store->Expensese->List-Action) เซ็ทค่าเข้าไปใน Object สำหรับ บันทึกลง Database
      const objForupdate = {
        name: val.formVal.name,
        auth: val.formVal.auth,
      }
      console.log('Server Edit OBJ: ', objForupdate)

      /*เพิ่ม 3.2 (store->Expensese->List-Action) เซ็ทค่าเข้าไปใน Object สำหรับ Commit เข้า State
        หมายเหตุ ไม่สามารถ Commit val ตรงๆได้เนื่องจากมี Data val.typeExpenses.auth อยู่ 
      */

      const objForcommit = {
        id: val.formVal.id,
        name: val.formVal.name,
        auth: val.formVal.auth,
      }

      await db
        .collection('expensesUnit')
        .doc(`${val.formVal.id}`)
        .set(objForupdate)
        .then(() => {
          console.log('Serve Update Success')
          commit('SET_Expenses_Unit_Update', objForcommit)
        })
        .catch((err) => console.log('Serve Update ExpensesList Error :', err))
    } catch (error) {
      throw error
    }
  },
  async delDataExpensesUnit({ commit }, deleteItem) {
    try {
      await db
        .collection('expensesUnit')
        .doc(`${deleteItem.id}`)
        .delete()
        .then(() => {
          console.log('Serve Delete Expenses Unit Success')
          commit('SET_Expenses_Unit_Delete', deleteItem)
        })
    } catch (error) {
      throw error
    }
  },
}
