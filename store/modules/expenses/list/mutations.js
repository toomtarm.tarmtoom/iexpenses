export default {
  SET_Expenses_List: (state, newTypeAll) => {
    state.ExpensesList = newTypeAll
  },


  // เพิ่ม 4.2 (store->Expensese->List-Mutation) เพิ่มข้อมูลเข้า State
  SET_Expenses_List_Add: (state, newVal) => {
     // เพิ่มข้อมูล state.ExpensesType
    state.ExpensesList.push(newVal)
  },

  SET_Expenses_List_Update: (state, newObj) => { // แก้ไข อัพเดท state.ExpensesList
    const indexNumber = state.ExpensesList.findIndex((stateIndex) => stateIndex.id === newObj.id) // หา Index แล้ว -1
    Object.assign(state.ExpensesList[indexNumber], newObj)
  },

  
  SET_Expenses_List_EditData: (state, newVal) =>{
    state.EditData = newVal // สำหรับส่งข้อมูลข้า Component
  },

  SET_Expenses_List_LastID: (state, newID) => {
    //ค้นหา ID ล่าสุด
    state.ListID = newID
  },

  //เพิ่ม 4.3 (store->Expensese->List-Mutation) เพิ่ม ID + 1 ล่าสุด  เข้า State สำหรับ ID 
  SET_Expenses_List_LastID1: (state, newVal) => {
    //ค้นหา ID ล่าสุด +1
    state.ListID = newVal + 1
  },
  SET_Expenses_List_EditItem: (state, newItem) =>{
    //จัดการระหว่างเพิ่ม/แก้ไขข้อมูล  เพิ่ม -1 / แก้ไข 1
    state.EditItem = newItem
  },

  //ลบ 3.1 (store->Expensese->List-Mutation) ใช้ Fnc ในการค้นหา ID และทำการลบข้อมูล Where ID ที่ค้นหาออกจาก State
  SET_Expenses_List_Delete: (state, newObj) => {
    const indexNumber = state.ExpensesList.findIndex((stateIndex) => stateIndex.id === newObj.id) // หา Index แล้ว -1
    state.ExpensesList.splice(indexNumber, 1)
  },

  /*


     Template
   */

  //เพิ่ม 4.1 (store->Expensese->List-Mutation) ปิดหน้า Form

  SET_FORM_EXPENSES_LIST: (state, newExpand) => {
    // Toggle Component
    state.formExpensesList = newExpand
  },
}