export default {
  getExpensesList: state => state.ExpensesList,
  getExpensesListID: state => state.ListID
}