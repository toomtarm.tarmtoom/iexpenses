import { db, auth } from '@/services/firebase'

export default {
  callListAll: ({ commit }, id) => {
    auth.onAuthStateChanged((user) => {
      db.collection('expensesList')
        .where('auth', 'array-contains', `${user.email}`)
        .onSnapshot((querySnapshot) => {
          let dataAr = []
          querySnapshot.forEach((doc) => {
            var whereID = doc.data().extypeID

            db.collection('expensesType')
              .doc(`${whereID}`)
              .get()
              .then((doc1) => {
                const currentData = {
                  id: doc.id,
                  name: doc.data().exlistName,
                  detail: doc.data().exlistDetail,
                  auth: doc.data().auth,
                  typeExpenses: {
                    id: doc.data().extypeID,
                    name: doc1.data().extypeName,
                    detail: doc1.data().extypeDetail,
                  },
                }
                dataAr = [...dataAr, currentData]
                commit('SET_Expenses_List', dataAr)
              })
          })
        })
    })
  },
  findID: ({ commit }) => {
    db.collection('expensesList')
      .get()
      .then((querySnapshot) => {
        if (!querySnapshot.empty) {
          let last = []
          querySnapshot.forEach((doc) => {
            last = [...last, parseInt(doc.id)]
          })
          commit(
            'SET_Expenses_List_LastID',
            last.reduce((prev, cur) => (cur > prev ? cur : prev)) + 1
          )
        } else {
          var valID = 1
          commit('SET_Expenses_List_LastID', valID)
        }
      })
  },
  async addDataExpensesList(
    {
      //เพิ่มข้อมูล
      commit,
    },
    val
  ) {
    try {
      //เพิ่ม 3.1 (store->Expensese->List-Action) เซ็ทค่าเข้าไปใน Object สำหรับ บันทึกลง Database
      const objForsave = {
        exlistName: val.formVal.name,
        exlistDetail: val.formVal.detail,
        extypeID: val.formVal.typeExpenses.id,
        auth: [val.formVal.auth],
      }

      /*เพิ่ม 3.2 (store->Expensese->List-Action) เซ็ทค่าเข้าไปใน Object สำหรับ Commit เข้า State
        หมายเหตุ ไม่สามารถ Commit val ตรงๆได้เนื่องจากมี Data val.typeExpenses.auth อยู่ 
      */
      const objForcommit = {
        id: val.formVal.id,
        name: val.formVal.name,
        detail: val.formVal.detail,
        auth: [val.formVal.auth],
        typeExpenses: {
          id: val.formVal.typeExpenses.id,
          name: val.formVal.typeExpenses.name,
          detail: val.formVal.typeExpenses.detail,
        },
      }
      //เพิ่ม 3.3 (store->Expensese->List-Action) คำสั่งบันทึกลงใน Database
      await db
        .collection('expensesList')
        .doc(`${val.formVal.id}`)
        .set(objForsave)
        .then(() => {
          console.log('Serve ADD ExpensesType Sussess')

          //เพิ่ม 3.3.1 (store->Expensese->List-Action) Set Commit สำหรับเพิ่มข้อมูล ExpensesList
          commit('SET_Expenses_List_Add', objForcommit)

          //เพิ่ม 3.3.2 (store->Expensese->List-Action) Set Commit จัดการ ID ล่าสุด  เข้า State สำหรับ ID
          commit('SET_Expenses_List_LastID1', val.formVal.id)
        })
        .catch((err) => console.log('Serve ADD ExpensesType Error :', err))
      // commit("SET_USER", {
      //   email,
      //   uid
      // });
    } catch (error) {
      throw error
    }
  },
  async editDataExpensesList({ commit }, val) {
    try {
      //เพิ่ม 3.1 (store->Expensese->List-Action) เซ็ทค่าเข้าไปใน Object สำหรับ บันทึกลง Database
      const objForupdate = {
        exlistName: val.formVal.name,
        exlistDetail: val.formVal.detail,
        extypeID: val.formVal.typeExpenses.id,
        auth: val.formVal.auth,
      }

      /*เพิ่ม 3.2 (store->Expensese->List-Action) เซ็ทค่าเข้าไปใน Object สำหรับ Commit เข้า State
        หมายเหตุ ไม่สามารถ Commit val ตรงๆได้เนื่องจากมี Data val.typeExpenses.auth อยู่ 
      */
      const objForcommit = {
        id: val.formVal.id,
        name: val.formVal.name,
        detail: val.formVal.detail,
        auth: val.formVal.auth,
        typeExpenses: {
          id: val.formVal.typeExpenses.id,
          name: val.formVal.typeExpenses.name,
          detail: val.formVal.typeExpenses.detail,
        },
      }

      await db
        .collection('expensesList')
        .doc(`${val.formVal.id}`)
        .set(objForupdate)
        .then(() => {
          console.log('Serve Update Success')
          commit('SET_Expenses_List_Update', objForcommit)
        })
        .catch((err) => console.log('Serve Update ExpensesList Error :', err))
    } catch (error) {
      throw error
    }
  },
  async delDataExpensesList({ commit }, val) {
    try {
      //ลบ 2.1 (store->Expensese->List-Action) ลบข้อมูลใน Database
      await db
        .collection('expensesList')
        .doc(`${val.id}`)
        .delete()
        .then(() => {
          console.log('Serve Delete Expenses List Success')
          //ลบ 2.1 (store->Expensese->List-Action) Set Commit "val" เข้า State สำหรับลบข้อมูลใน State

          ///////////// ต้องกลับมาแก้ไข โดนพยายามสร้าง Function แยกออกมาได้ไหม
          db.collection('expensesList')
            .get()
            .then((querySnapshot) => {
              let last = []
              querySnapshot.forEach((doc) => {
                last = [...last, parseInt(doc.id)]
              })
              let getID =
                last.reduce((prev, cur) => (cur > prev ? cur : prev)) + 1
              commit('SET_Expenses_List_LastID', getID)
            })

          commit('SET_Expenses_List_Delete', val)
        })
    } catch (error) {
      throw error
    }
  },
}

getID: () => {
  db.collection('expensesList')
    .get()
    .then((querySnapshot) => {
      let last = []
      querySnapshot.forEach((doc) => {
        last = [...last, parseInt(doc.id)]
      })
      return last.reduce((prev, cur) => (cur > prev ? cur : prev)) + 1
    })
}
