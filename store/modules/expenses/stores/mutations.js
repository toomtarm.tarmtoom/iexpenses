export default {
  SET_Expenses_Stores: (state, newStores) => {
    state.ExpensesStores = newStores
  },
  // เพิ่ม 4.2 (store->Expensese->Table-Mutation) เพิ่มข้อมูลเข้า State
  SET_Expenses_Stores_Add: (state, newVal) => {
     // เพิ่มข้อมูล state.ExpensesType
    state.ExpensesStores.push(newVal)
  },

  SET_Expenses_Stores_Update: (state, newObj) => { // แก้ไข อัพเดท state.ExpensesStores
    const indexNumber = state.ExpensesStores.findIndex((stateIndex) => stateIndex.id === newObj.id) // หา Index แล้ว -1
    Object.assign(state.ExpensesStores[indexNumber], newObj)
  },

  
  SET_Expenses_Stores_EditData: (state, newVal) =>{
    state.EditData = newVal // สำหรับส่งข้อมูลข้า Component
  },

  SET_Expenses_Stores_LastID: (state, newID) => {
    //ค้นหา ID ล่าสุด
    state.StoresID = newID
  },

  //เพิ่ม 4.3 (store->Expensese->Stores-Mutation) เพิ่ม ID + 1 ล่าสุด  เข้า State สำหรับ ID 
  SET_Expenses_Stores_LastID1: (state, newVal) => {
    //ค้นหา ID ล่าสุด +1
    state.StoresID = newVal + 1
  },
  SET_Expenses_Stores_EditItem: (state, newItem) =>{
    //จัดการระหว่างเพิ่ม/แก้ไขข้อมูล  เพิ่ม -1 / แก้ไข 1
    state.EditItem = newItem
  },

  //ลบ 3.1 (store->Expensese->Stores-Mutation) ใช้ Fnc ในการค้นหา ID และทำการลบข้อมูล Where ID ที่ค้นหาออกจาก State
  SET_Expenses_Stores_Delete: (state, newObj) => {
    const indexNumber = state.ExpensesStores.findIndex((stateIndex) => stateIndex.id === newObj.id) // หา Index แล้ว -1
    state.ExpensesStores.splice(indexNumber, 1)
  },
  // SET_Expenses_Stores_Delete_ID: (state, valID) => {
  //   if(valID == state.StoresID){
  //     state.StoresID = (valID - 1)
  //   }
    
  // },
 
  /*
     Template
   */

  //เพิ่ม 4.1 (store->Expensese->Stores-Mutation) ปิดหน้า Form


  SET_FORM_EXPENSES_STORES: (state, newExpand) => {
    // Toggle Component
    state.formExpensesStores = newExpand
  },
  
}
