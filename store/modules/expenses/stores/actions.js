import { db, auth } from '@/services/firebase'

export default {
  callStores: ({ commit }) => {
    auth.onAuthStateChanged((user) => {
      db.collection('expensesStores')
        .where('auth', 'array-contains', `${user.email}`)
        .onSnapshot((querySnapshot) => {
          let dataAr = []
          querySnapshot.forEach((doc) => {
            const currentData = {
              id: doc.id,
              name: doc.data().exstoresName,
              location: doc.data().exstoresLocation,
              detail: doc.data().exstoresDetail,
              auth: doc.data().auth,
            }
            dataAr = [...dataAr, currentData]
            commit('SET_Expenses_Stores', dataAr)
          })
        })
    })
  },
  findID: ({ commit }) => {
    db.collection('expensesStores')
      .get()
      .then((querySnapshot) => {
        if (!querySnapshot.empty) {
          let last = []
          querySnapshot.forEach((doc) => {
            last = [...last, parseInt(doc.id)]
          })
          commit(
            'SET_Expenses_Stores_LastID',
            last.reduce((prev, cur) => (cur > prev ? cur : prev)) + 1
          )
        } else {
          var valID = 1
          commit('SET_Expenses_Stores_LastID', valID)
        }
      })
  },
  async addDataexpensesStores({ commit }, val) {
    try {
      const objForsave = {
        exstoresName: val.formVal.name,
        exstoresDetail: val.formVal.detail,
        exstoresLocation: val.formVal.location,
        auth: [val.formVal.auth],
      }

      const objForcommit = {
        id: val.formVal.id,
        name: val.formVal.name,
        detail: val.formVal.detail,
        location: val.formVal.location,
        auth: [val.formVal.auth],
      }

      await db
        .collection('expensesStores')
        .doc(`${val.formVal.id}`)
        .set(objForsave)
        .then(() => {
          console.log('Serve ADD ExpensesStores Sussess')

          //เพิ่ม 3.3.1 (store->Expensese->Stores-Action) Set Commit สำหรับเพิ่มข้อมูล ExpensesStores
          commit('SET_Expenses_Stores_Add', val.formVal)

          //เพิ่ม 3.3.2 (store->Expensese->Stores-Action) Set Commit จัดการ ID ล่าสุด  เข้า State สำหรับ ID
          commit('SET_Expenses_Stores_LastID1', val.formVal.id)
        })
        .catch((err) => console.log('Serve ADD ExpensesType Error :', err))
    } catch (error) {
      throw error
    }
  },
  async editDataExpensesStores({ commit }, val) {
    try {
      const obj = {
        exstoresName: val.formVal.name,
        exstoresDetail: val.formVal.detail,
        exstoresLocation: val.formVal.location,
        auth: [val.formVal.auth],
      }
      await db
        .collection('expensesStores')
        .doc(`${val.formVal.id}`)
        .set(obj)
        .then(() => {
          console.log('Serve Update Expenses Stores Success')
          commit('SET_Expenses_Stores_Update', val.formVal)
        })
        .catch((err) => console.log('Serve Update expensesStore Error :', err))
    } catch (error) {
      throw error
    }
  },
  async delDataexpensesStore({ commit }, deleteItem) {
    try {
      await db
        .collection('expensesStores')
        .doc(`${deleteItem.id}`)
        .delete()
        .then(() => {
          commit('SET_Expenses_Stores_Delete', deleteItem)
          db.collection('expensesStores')
            .get()
            .then((querySnapshot) => {
              if (!querySnapshot.empty) {
                let last = []
                querySnapshot.forEach((doc) => {
                  last = [...last, parseInt(doc.id)]
                })
                commit(
                  'SET_Expenses_Stores_LastID',
                  last.reduce((prev, cur) => (cur > prev ? cur : prev)) + 1
                )
              } else {
                var valID = 1
                commit('SET_Expenses_Stores_LastID', valID)
              }
            })
        })
    } catch (error) {
      throw error
    }
  },
}
