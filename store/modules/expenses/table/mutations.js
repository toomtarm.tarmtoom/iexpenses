export default {
  SET_Expenses_Table: (state, newTypeAll) => {
    state.ExpensesTable = newTypeAll
  },


  // เพิ่ม 4.2 (store->Expensese->Table-Mutation) เพิ่มข้อมูลเข้า State
  SET_Expenses_Table_Add: (state, newVal) => {
     // เพิ่มข้อมูล state.ExpensesType
    state.ExpensesTable.push(newVal)
  },

  SET_Expenses_Table_Update: (state, newObj) => { // แก้ไข อัพเดท state.ExpensesTable
    const indexNumber = state.ExpensesTable.findIndex((stateIndex) => stateIndex.id === newObj.id) // หา Index แล้ว -1
    Object.assign(state.ExpensesTable[indexNumber], newObj)
  },

  
  SET_Expenses_Table_EditData: (state, newVal) =>{
    state.EditData = newVal // สำหรับส่งข้อมูลข้า Component
  },

  SET_Expenses_Table_LastID: (state, newID) => {
    //ค้นหา ID ล่าสุด
    state.TableID = newID
  },

  //เพิ่ม 4.3 (store->Expensese->Table-Mutation) เพิ่ม ID + 1 ล่าสุด  เข้า State สำหรับ ID 
  SET_Expenses_Table_LastID1: (state, newVal) => {
    //ค้นหา ID ล่าสุด +1
    state.TableID = newVal + 1
  },
  SET_Expenses_Table_EditItem: (state, newItem) =>{
    //จัดการระหว่างเพิ่ม/แก้ไขข้อมูล  เพิ่ม -1 / แก้ไข 1
    state.EditItem = newItem
  },

  //ลบ 3.1 (store->Expensese->Table-Mutation) ใช้ Fnc ในการค้นหา ID และทำการลบข้อมูล Where ID ที่ค้นหาออกจาก State
  SET_Expenses_Table_Delete: (state, newObj) => {
    const indexNumber = state.ExpensesTable.findIndex((stateIndex) => stateIndex.id === newObj.id) // หา Index แล้ว -1
    state.ExpensesTable.splice(indexNumber, 1)
  },

  SET_Expenses_Table_Last_date: (state, newVal) =>{
    state.tableLastDate = newVal
  },
 
  /*
     Template
   */

  //เพิ่ม 4.1 (store->Expensese->Table-Mutation) ปิดหน้า Form


  SET_FORM_EXPENSES_TABLE: (state, newExpand) => {
    // Toggle Component
    state.formExpensesTable = newExpand
  },
  
}
