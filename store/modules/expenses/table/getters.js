export default {
  getExpensesTable: (state) => state.ExpensesTable,
  getExpensesTableID: (state) => state.TableID,
  getExpensesTableLastDate: (state) => state.tableLastDate,
}
