import { db, auth } from '@/services/firebase'

export default {
  // citiesRef.orderBy("name", "desc").limit(3);
  async callLastVal({
    //เพิ่มข้อมูล
    commit,
  }) {
    auth.onAuthStateChanged((user) => {
      db.collection('expensesTable')
        .where('auth', 'array-contains', `${user.email}`)
        .orderBy('extableDate', 'asc')
        .limit(1)
        .onSnapshot((querySnapshot) => {
          querySnapshot.forEach((doc) => {
            const currentData = {
              date: doc.data().extableDate,
            }
            // console.log('serverDoc', doc.data().extableDate)
            commit('SET_Expenses_Table_Last_date', currentData)
          })
        })
    })
  },
  async callTableAll({
    //เพิ่มข้อมูล
    commit,
  }) {
    auth.onAuthStateChanged((user) => {
      db.collection('expensesTable')
        .where('auth', 'array-contains', `${user.email}`)
        .onSnapshot((querySnapshot) => {
          let dataAr = []
          querySnapshot.forEach((doc) => {
            var whereID = doc.data().exlistID
            var whereUnitID = doc.data().exunitID
            var whereStoresID = doc.data().exstoresID

            db.collection('expensesList')
              .doc(`${whereID}`)
              .get()
              .then((doc1) => {
                var whereTypeID = doc1.data().extypeID
                db.collection('expensesStores')
                  .doc(`${whereStoresID}`)
                  .get()
                  .then((doc2) => {
                    db.collection('expensesUnit')
                      .doc(`${whereUnitID}`)
                      .get()
                      .then((doc3) => {
                        db.collection('expensesType')
                          .doc(`${whereTypeID}`)
                          .get()
                          .then((doc4) => {
                            const currentData = {
                              id: doc.id,
                              no: doc.data().extableNo,
                              doc: doc.data().exdocID,
                              listExpenses: {
                                id: doc1.id,
                                name: doc1.data().exlistName,
                                detail: doc1.data().exlistDetail,
                              },
                              typeExpenses: {
                                id: doc4.id,
                                name: doc4.data().extypeName,
                                detail: doc4.data().extypeDetail,
                              },
                              storesExpenses: {
                                id: doc2.id,
                                name: doc2.data().exstoresName,
                                detail: doc2.data().exstoresDetail,
                                location: doc2.data().exstoresLocation,
                              },
                              amount: doc.data().extableAmount,
                              unitExpenses: {
                                id: doc3.id,
                                name: doc3.data().name,
                              },
                              price: doc.data().extablePrice,
                              date: new Date(doc.data().extableDate)
                                .toISOString()
                                .substr(0, 10),
                              detail: doc.data().extableDetail,
                              auth: doc.data().auth,
                            }
                            dataAr = [...dataAr, currentData]
                            commit('SET_Expenses_Table', dataAr)
                          })
                      })
                  })
              })
          })
        })
    })
  },

  findID: ({ commit }) => {
    db.collection('expensesTable')
      .get()
      .then((querySnapshot) => {
        if (!querySnapshot.empty) {
          let last = []
          querySnapshot.forEach((doc) => {
            last = [...last, parseInt(doc.id)]
          })
          commit(
            'SET_Expenses_Table_LastID',
            last.reduce((prev, cur) => (cur > prev ? cur : prev)) + 1
          )
        } else {
          var valID = 1
          commit('SET_Expenses_Table_LastID', valID)
        }
      })
  },
  async addDataExpensesTable(
    {
      //เพิ่มข้อมูล
      commit,
    },
    val
  ) {
    try {
      //เพิ่ม 3.1 (store->Expensese->List-Action) เซ็ทค่าเข้าไปใน Object สำหรับ บันทึกลง Database

      let dateTimestamp = new Date(val.formVal.date).getTime()

      const objForsave = {
        extableNo: val.formVal.no,
        exlistID: val.formVal.listExpenses.id,
        extableDetail: val.formVal.detail,
        extableAmount: val.formVal.amount,
        extableDate: dateTimestamp,
        extablePrice: val.formVal.price,
        exunitID: val.formVal.unitExpenses.id,
        exdocID: val.formVal.doc,
        exstoresID: val.formVal.storesExpenses.id,
        auth: [val.formVal.auth],
      }
      // console.log('Server ObjForsave', objForsave)

      /*เพิ่ม 3.2 (store->Expensese->List-Action) เซ็ทค่าเข้าไปใน Object สำหรับ Commit เข้า State
        หมายเหตุ ไม่สามารถ Commit val ตรงๆได้เนื่องจากมี Data val.typeExpenses.auth อยู่ 
      */
      const objForcommit = {
        id: val.formVal.id,
        no: val.formVal.no,
        doc: val.formVal.doc,
        listExpenses: {
          id: val.formVal.listExpenses.id,
          name: val.formVal.listExpenses.name,
          detail: val.formVal.listExpenses.detail,
        },
        amount: val.formVal.amount,
        unitExpenses: {
          id: val.formVal.unitExpenses.id,
          name: val.formVal.unitExpenses.name,
        },
        storesExpenses: {
          id: val.formVal.storesExpenses.id,
          name: val.formVal.storesExpenses.name,
          detail: val.formVal.storesExpenses.detail,
          location: val.formVal.storesExpenses.location,
        },
        price: val.formVal.price,
        date: val.formVal.date,
        detail: val.formVal.detail,
        auth: [val.formVal.auth],
      }

      // console.log('Server objForcommit', objForcommit)

      //เพิ่ม 3.3 (store->Expensese->List-Action) คำสั่งบันทึกลงใน Database

      await db
        .collection('expensesTable')
        .doc(`${val.formVal.id}`)
        .set(objForsave)
        .then(() => {
          console.log('Serve ADD ExpensesTable Sussess')

          //เพิ่ม 3.3.1 (store->Expensese->List-Action) Set Commit สำหรับเพิ่มข้อมูล ExpensesList
          commit('SET_Expenses_Table_Add', objForcommit)

          //เพิ่ม 3.3.2 (store->Expensese->List-Action) Set Commit จัดการ ID ล่าสุด  เข้า State สำหรับ ID
          commit('SET_Expenses_Table_LastID1', val.formVal.id)
        })
        .catch((err) => console.log('Serve ADD ExpensesType Error :', err))
    } catch (error) {
      throw error
    }
  },
  async editDataExpensesTable({ commit }, val) {
    try {
      //เพิ่ม 3.1 (store->Expensese->List-Action) เซ็ทค่าเข้าไปใน Object สำหรับ บันทึกลง Database

      // console.log('Server ObjForsave', val.formVal)

      let dateTimestamp = new Date(val.formVal.date).getTime()

      const objForupdate = {
        extableNo: val.formVal.no,
        exlistID: val.formVal.listExpenses.id,
        extableDetail: val.formVal.detail,
        extableAmount: val.formVal.amount,
        extableDate: dateTimestamp,
        extablePrice: val.formVal.price,
        exunitID: val.formVal.unitExpenses.id,
        exdocID: val.formVal.doc,
        exstoresID: val.formVal.storesExpenses.id,
        auth: [val.formVal.auth],
      }

      /*เพิ่ม 3.2 (store->Expensese->List-Action) เซ็ทค่าเข้าไปใน Object สำหรับ Commit เข้า State
        หมายเหตุ ไม่สามารถ Commit val ตรงๆได้เนื่องจากมี Data val.typeExpenses.auth อยู่
      */
      const objForcommit = {
        id: val.formVal.id,
        no: val.formVal.no,
        listExpenses: {
          id: val.formVal.listExpenses.id,
          name: val.formVal.listExpenses.name,
          detail: val.formVal.listExpenses.detail,
        },
        amount: val.formVal.amount,
        unitExpenses: {
          id: val.formVal.unitExpenses.id,
          name: val.formVal.unitExpenses.name,
        },
        storesExpenses: {
          id: val.formVal.storesExpenses.id,
          name: val.formVal.storesExpenses.name,
          detail: val.formVal.storesExpenses.detail,
          location: val.formVal.storesExpenses.location,
        },
        price: val.formVal.price,
        date: val.formVal.date,
        detail: val.formVal.detail,
        auth: [val.formVal.auth],
      }

      await db
        .collection('expensesTable')
        .doc(`${val.formVal.id}`)
        .set(objForupdate)
        .then(() => {
          console.log('Serve Update Success')
        })
        .catch((err) => console.log('Serve Update ExpensesTable Error :', err))
    } catch (error) {
      throw error
    }
  },
  async delDataexpensesTable({ commit }, val) {
    try {
      //ลบ 2.1 (store->Expensese->List-Action) ลบข้อมูลใน Database
      await db
        .collection('expensesTable')
        .doc(`${val.id}`)
        .delete()
        .then(() => {
          console.log('Serve Delete Expenses Type Success')

          //ลบ 2.1 (store->Expensese->List-Action) Set Commit "val" เข้า State สำหรับลบข้อมูลใน State
          commit('SET_Expenses_Table_Delete', val)
          db.collection('expensesTable')
            .get()
            .then((querySnapshot) => {
              if (!querySnapshot.empty) {
                let last = []
                querySnapshot.forEach((doc) => {
                  last = [...last, parseInt(doc.id)]
                })
                commit(
                  'SET_Expenses_Table_LastID',
                  last.reduce((prev, cur) => (cur > prev ? cur : prev)) + 1
                )
              } else {
                var valID = 1
                commit('SET_Expenses_Table_LastID', valID)
              }
            })
        })
    } catch (error) {
      throw error
    }
  },
}
