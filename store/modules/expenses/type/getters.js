export default {
  getExpensesType: state => state.ExpensesType,
  getTypeID: state => state.TypeID
}