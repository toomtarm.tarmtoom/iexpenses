import { db, auth } from '@/services/firebase'

export default {
  callTypeAll: ({ commit }, id) => {
   auth.onAuthStateChanged((user) => {
      db.collection('expensesType')
        .where('auth', 'array-contains', `${user.email}`)
        .onSnapshot((querySnapshot) => {
          let dataAr = []
          querySnapshot.forEach((doc) => {
            const currentData = {
              id: doc.id,
              name: doc.data().extypeName,
              detail: doc.data().extypeDetail,
              auth: doc.data().auth,
            }
            dataAr = [...dataAr, currentData]
            commit('SET_Expenses_Type', dataAr)
          })
        })
    })
  },
  findID: ({ commit }) => {
    db.collection('expensesType')
      .get()
      .then((querySnapshot) => {
        if (!querySnapshot.empty) {
          /*
          var last =
            querySnapshot.docs[
              Object.keys(querySnapshot.docs)[
                Object.keys(querySnapshot.docs).length - 1
              ]
            ]
          var valID = parseInt(last.id) + 1
          console.log('ID :', valID)
          commit('SET_Expenses_TypeID', valID)
          */
          let last = []
          querySnapshot.forEach((doc) => {
            last = [...last, parseInt(doc.id)]
          })
          commit(
            'SET_Expenses_TypeID',
            last.reduce((prev, cur) => (cur > prev ? cur : prev)) + 1
          )
        } else {
          var valID = 1
          commit('SET_Expenses_TypeID', valID)
        }
      })
  },
  async addDataExpensesType({ commit }, editedItem) {
    try {
      const obj = {
        extypeName: editedItem.name,
        extypeDetail: editedItem.detail,
        auth: [editedItem.auth],
      }
      const objDefaultItem = {
        id: editedItem.id,
        name: editedItem.name,
        detail: editedItem.detail,
        auth: [editedItem.auth],
      }
      // console.log('Server Before ID: ', editedItem.id)
      await db
        .collection('expensesType')
        .doc(`${editedItem.id}`)
        .set(obj)
        .then(() => {
          console.log('Serve ADD ExpensesType Sussess')
          commit('SET_Expenses_Type_Add', objDefaultItem)
          commit('SET_ADDCOSTTYPEID', editedItem.id)
        })
        .catch((err) => console.log('Serve ADD ExpensesType Error :', err))
      // commit("SET_USER", {
      //   email,
      //   uid
      // });
    } catch (error) {
      throw error
    }
  },
  async editDataExpensesType({ commit }, editedItem) {
    try {
      const obj = {
        extypeName: editedItem.name,
        extypeDetail: editedItem.detail,
        auth: ['test@test.com'],
      }
      // console.log('Auth :', editedItem)
      // console.log('OBJ : ', obj)
      // console.log("ID is : ", findIndex(editedItem))
      // Object.assign(this.tableData[this.editedIndex], this.editedItem)
      await db
        .collection('expensesType')
        .doc(`${editedItem.id}`)
        .set(obj)
        .then(() => {
          console.log('Serve Update Expenses Type Success')
          commit('SET_Expenses_Type_Update', editedItem)
        })
        .catch((err) => console.log('Serve Update ExpensesType Error :', err))
    } catch (error) {
      throw error
    }
  },
  async delDataExpensesType({ commit }, deleteItem) {
    try {
      await db
        .collection('expensesType')
        .doc(`${deleteItem.id}`)
        .delete()
        .then(() => {
          console.log('Serve Delete Expenses Type Success')
          commit('SET_Expenses_Type_Delete', deleteItem)
        })
    } catch (error) {
      throw error
    }
  },
}
