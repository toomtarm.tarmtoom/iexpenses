export default {
  SET_Expenses_Type: (state, newTypeAll) => {
    // console.log("server :", newTypeAll)
    state.ExpensesType = newTypeAll
  },
  SET_Expenses_Type_Add: (state, newVal) => { // เพิ่มข้อมูล state.ExpensesType
    state.ExpensesType.push(newVal)
    // state.ExpensesType = {
    //   ...state.ExpensesType,
    //   newVal
    // };
  },
  SET_Expenses_Type_Update: (state, newObj) => { // แก้ไข อัพเดท state.ExpensesType
    const indexNumber = state.ExpensesType.findIndex((stateIndex) => stateIndex.id === newObj.id) // หา Index แล้ว -1
    Object.assign(state.ExpensesType[indexNumber], newObj)
  },
  SET_Expenses_Type_Delete: (state, newObj) => {
    const indexNumber = state.ExpensesType.findIndex((stateIndex) => stateIndex.id === newObj.id) // หา Index แล้ว -1
    state.ExpensesType.splice(indexNumber, 1)
  },
  SET_ADDCOSTTYPEID: (state, newVal) => {
    state.TypeID = newVal + 1
  },
  SET_Expenses_TypeID: (state, newID) => {
    //ค้นหา ID ล่าสุด
    state.TypeID = newID
  },

  SET_Expenses_TYPE_EditData: (state, newVal) =>{
    state.EditData = newVal // สำหรับส่งข้อมูลข้า Component
  },

  SET_Expenses_TYPE_EditItem: (state, newItem) =>{
    //จัดการระหว่างเพิ่ม/แก้ไขข้อมูล  เพิ่ม -1 / แก้ไข 1
    state.EditItem = newItem
  },
  /*


    Template
  */
 SET_FORM_EXPENSES_TYPE: (state, newExpand) => {
    // Toggle Component
    state.expand = newExpand
  },
}
