//import * as firebase from 'firebase'
import firebase from 'firebase/app'
import 'firebase/auth'
import 'firebase/database'
import 'firebase/firestore'
import 'firebase/storage'

const config = {
    apiKey: 'AIzaSyBiXPxfUOFD_avpAD8T7UcdMaEaKuMBeJQ',
      authDomain: 'iexpenses-project.firebaseapp.com',
      databaseURL: 'https://iexpenses-project.firebaseio.com',
      projectId: 'iexpenses-project',
      storageBucket: 'iexpenses-project.appspot.com',
      messagingSenderId: '421169111251',
      appId: '1:421169111251:web:69118aed33fb7add07f426',
      measurementId: 'G-NBGT3TB07F',
    }
  // Initialize Firebase
  // firebase.initializeApp(config)
  // firebase.analytics()

  !firebase.apps.length ? firebase.initializeApp(config) : ''
  export const auth = firebase.auth();
  export const db = firebase.firestore();
  export const googleProvider = new firebase.auth.GoogleAuthProvider();
  export const facebookProvider = new firebase.auth.FacebookAuthProvider();
  export const storage = firebase.storage().ref();
